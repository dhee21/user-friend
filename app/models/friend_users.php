<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class friend_users extends Model
{
    //
     public function fromUser()
    {
        return $this->belongsTo('App\Models\User', 'sender')->select(array('id', 'name'));
    }

    public function toUser()
    {
        return $this->belongsTo('App\Models\User', 'receiver')->select(array('id', 'name'));
        
    }
}
