<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\models\User;
use App\models\friend_users;

use App\Http\Requests;
use Auth;

class mainController extends Controller
{
	public function __construct() {
    	$this->middleware('auth');
  	}
    //
    // tells the friends of logged in user
    public function tellFriends (Request $request) {
    	$userid = Auth::user()->id;
        $query =  friend_users::with(['fromUser' , 'toUser'])->where('connected' , 1);
            $answer = $query->where(function($query) use($userid){
                $query->where('sender' , $userid)
                        ->orWhere('receiver' , $userid);
            })->orderBy('updated_at' , 'desc')->get(['id' , 'sender' , 'receiver', 'connected']);
    	return response()->json([ 'name' => 'friends', 'data' => $answer ]);
    }
    public function sentRequests(Request $request) {
        $userid = Auth::user()->id;
        $query =  friend_users::with(['fromUser' , 'toUser'])->where('connected' , 0);
            $answer = $query->where(function($query) use($userid){
                $query->where('sender' , $userid);
            })->orderBy('updated_at' , 'desc')->get(['id' , 'sender' , 'receiver', 'connected']);
        return response()->json([ 'name' => 'sent Requests', 'data' => $answer ]);
    }
    public function recievedRequests(Request $request) {
        $userid = Auth::user()->id;
        $query =  friend_users::with(['fromUser' , 'toUser'])->where('connected' , 0);
            $answer = $query->where(function($query) use($userid){
                $query->where('receiver' , $userid);
            })->orderBy('updated_at' , 'desc')->get(['id' , 'sender' , 'receiver', 'connected']);
        return response()->json([ 'name' => 'recieved requests', 'data' => $answer ]);
    }
    // user_id is the id of user to which requet is sent
    public function sendRequest(Request $request, $user_id) {
        $friendUser = new friend_users;
        $friendUser->sender = Auth::user()->id;
        $friendUser->receiver = $user_id;
        $friendUser->connected = 0;
        $friendUser->save();
         return response()->json([ 'name' => 'request sended', 'data' => $friendUser  ]);
    }
    // user_id is the id of user whose request is to be accepted
    public function acceptRequest(Request $request, $user_id) {
        $friendUser = friend_users::where('sender', $user_id)->where('receiver', Auth::user()->id)->first();
        $friendUser->connected = 1;
        $friendUser->save();
        return response()->json([ 'name' => 'request accepted', 'data' => $friendUser  ]);
    }
}
