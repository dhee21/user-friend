<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', function () {
    return view('welcome');
});
// to see my friends
Route::get('/see_my_friends', ['as' => 'see_my_friends', 'uses' => 'mainController@tellFriends']);
// to see the request send by me
Route::get('/sent_requests', ['as' => 'sent_requests', 'uses' => 'mainController@sentRequests']);
// to se the requests recieved by me
Route::get('/recieved_requests', ['as' => 'recieved_requests', 'uses' => 'mainController@recievedRequests']);
// to send a request
Route::get('/send_request/{user_id}', ['as' => 'sendRequest', 'uses' => 'mainController@sendRequest']);
// to accept a request
Route::get('/accept_request/{user_id}', ['as' => 'acceptRequest', 'uses' => 'mainController@acceptRequest']);

Route::group(['middleware' => 'web'], function () {
	Route::auth();
	Route::get('/home', 'HomeController@index');

});

