<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFriendUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //

         Schema::create('friend_users', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('sender');
            $table->bigInteger('receiver');
            $table->bigInteger('connected');
            $table->timestamps();
        });
        //
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('friend_users');

    }
}
