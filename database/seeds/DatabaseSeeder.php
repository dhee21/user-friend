<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = array(
    	[
            'name' => "dheeraj",
            'email' => "dheerajvats5117".'@gmail.com',
            'password' => bcrypt('dheeraj'),
        ],
        [
        	'name' => "amit",
            'email' => "amit".'@gmail.com',
            'password' => bcrypt('amit'),
        ],
        [
        	'name' => "neeraj",
            'email' => "neeraj".'@gmail.com',
            'password' => bcrypt('neeraj'),
        ],
        [
        	'name' => "anurag",
            'email' => "anurag".'@gmail.com',
            'password' => bcrypt('anurag'),
        ],
        [
        	'name' => "akshat",
            'email' => "akshat".'@gmail.com',
            'password' => bcrypt('akshat'),
        ]);
        DB::table('users')->insert($users);
    }
}
